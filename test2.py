#!/usr/bin/env python3

import configparser
import datetime as dt
import os
import pprint

import solaredge

pp = pprint.PrettyPrinter(indent=2)

# read api_key and site_id from the file ~/.config/solaredge/account.ini
iniconf = configparser.ConfigParser()
iniconf.read(f"{os.environ['HOME']}/.config/solaredge/account.ini")
api_key = iniconf.get('account', 'api_key')

api = solaredge.Solaredge(api_key)

site_list = api.get_list()['sites']['site']
print("\n*****\nList of Sites\n")
# we only show the first four sites
pp.pprint(site_list[:3])

"""
The calls to various functions follows the order of the various API calls as they are listed in:
https://www.solaredge.com/sites/default/files/se_monitoring_api.pdf   (version: January 2019)
"""
# we only look at the first two sites
for site in site_list[:3]:
        # try:
        site_id = site['id']
        site_name = site['name']

        print(f"\n===============================================================\nSite {site_name} [{site_id}] Details\n")
        overview = api.get_overview(site_id)['overview']
        pp.pprint(overview)
        datetime = overview['lastUpdateTime']
        energy = overview['lifeTimeData']['energy']

        print(datetime, energy)

        vandaag = dt.datetime.now().strftime('%Y-%m-%d')
        print(vandaag)
        energi = api.get_energy_details_dataframe(site_id,
                                                  dt.datetime.strptime(f'{vandaag} 01:00:00', '%Y-%m-%d %H:%M:%S'),
                                                  dt.datetime.now(),
                                                  time_unit='QUARTER_OF_AN_HOUR'
                                                  )
        pp.pprint(energi)
        # except:
        continue

