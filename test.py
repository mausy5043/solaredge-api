#!/usr/bin/env python3

import configparser
import datetime
import os
import pprint

import solaredge

pp = pprint.PrettyPrinter(indent=2)

# read api_key and site_id from the file ~/.config/solaredge/account.ini
iniconf = configparser.ConfigParser()
iniconf.read(f"{os.environ['HOME']}/.config/solaredge/account.ini")
api_key = iniconf.get('account', 'api_key')

# calculate timeframe
start_time = (datetime.datetime.now() - datetime.timedelta(days=1)).strftime('%Y-%m-%d')
end_time = datetime.datetime.now().strftime('%Y-%m-%d')

print("\n*****\nRequesting Account Info\n*****\n")
print(start_time)
print(end_time)


# api supported version


api = solaredge.Solaredge(api_key)

site_list = api.get_list()['sites']['site']
print("\n*****\nList of Sites\n")
# we only show the first four sites
pp.pprint(site_list[:3])

"""
The calls to various functions follows the order of the various API calls as they are listed in:
https://www.solaredge.com/sites/default/files/se_monitoring_api.pdf   (version: January 2019)
"""
# we only look at the first two sites
for site in site_list[:1]:
    try:
        site_id = site['id']
        site_name = site['name']

        details = api.get_details(site_id)['details']
        print(f"\n===============================================================\nSite {site_name} [{site_id}] Details\n")
        pp.pprint(details)

        data_period = api.get_data_period(site_id)
        print(f"\nDataPeriod")
        data_period_start = data_period['dataPeriod']['startDate']
        data_period_end = data_period['dataPeriod']['endDate']
        print(f"Site data starts on : {data_period_start}")
        print(f"Site data ends on   : {data_period_end}")

        # site energy: function not (yet) part of this script

        time_frame_energy = api.get_time_frame_energy(site_id, start_time, data_period_end)
        print(f"\nEnergy Totaliser")
        pp.pprint(time_frame_energy)

        # site power: function not (yet) part of this script

        overview = api.get_overview(site_id)['overview']
        print(f"\nSite Overview")
        pp.pprint(overview)

        # site power details: function not (yet) part of this script

        # site energy details: function not (yet) part of this script

        current_power_flow = api.get_current_power_flow(site_id)['siteCurrentPowerFlow']
        print(f"\nCurrent Power Flow")
        pp.pprint(current_power_flow)

        # storage_data = api.get_storage_data(site_id, start_time, data_period_end)['storageData']
        # pp.pprint(f"\nStorage Information")
        # pp.pprint(storage_data)

        # site image: function not (yet) part of this script

        # site environmental benefits: function not (yet) part of this script

        # installer logo: function not (yet) part of this script

        # site components list: function not (yet) part of this script

        inventory_data = api.get_inventory(site_id)['Inventory']
        print(f"\nInventory")
        pp.pprint(inventory_data)


        # inverter technical data: function not (yet) part of this script

        # meters data

        # sensor list

        # sensor data

        # current api version



        # per day data
        energy = api.get_energy(site_id, start_time, data_period_end, time_unit='DAY')
        print(f"\nEnergy")
        pp.pprint(energy)
        print("\n\n")

        # how to get current timestamp with totaliser value:
        epoch = int(datetime.datetime.strptime(overview['lastUpdateTime'], "%Y-%m-%d %H:%M:%S").timestamp())
        production_totaliser = int(overview['lifeTimeData']['energy'])
        print(epoch, production_totaliser)

        #
        #
        #
        # # per time data
        # power = api.get_power(site_id, start_time, data_period_end)
        # powerdetails = api.get_power_details(site_id, start_time, end_time)
        # energydetails = api.get_energy_details(site_id, start_time, end_time)
    except:
        continue
